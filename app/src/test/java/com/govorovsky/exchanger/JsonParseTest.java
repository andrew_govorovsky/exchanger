package com.govorovsky.exchanger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.govorovsky.exchanger.api.CurrencyJsonDeserializer;
import com.govorovsky.exchanger.model.Currency;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class JsonParseTest {

    public static final String USD_RESPONSE = "{\"base\":\"USD\",\"date\":\"2017-09-01\",\"rates\":{\"AUD\":1.2602,\"BGN\":1.6408,\"BRL\":3.1395,\"CAD\":1.2441,\"CHF\":0.95982,\"CNY\":6.5591,\"CZK\":21.877,\"DKK\":6.2398,\"GBP\":0.77244,\"HKD\":7.8248,\"HRK\":6.2261,\"HUF\":255.95,\"IDR\":13318.0,\"ILS\":3.5737,\"INR\":64.034,\"JPY\":110.14,\"KRW\":1120.3,\"MXN\":17.836,\"MYR\":4.2705,\"NOK\":7.7647,\"NZD\":1.3958,\"PHP\":51.091,\"PLN\":3.5576,\"RON\":3.856,\"RUB\":57.737,\"SEK\":7.9512,\"SGD\":1.3545,\"THB\":33.17,\"TRY\":3.438,\"ZAR\":12.935,\"EUR\":0.83893}}";

    @Test
    public void testDeserialization() throws Exception {
        Gson gson = new GsonBuilder().registerTypeAdapter(Currency[].class, new CurrencyJsonDeserializer()).create();
        Currency[] currencies = gson.fromJson(USD_RESPONSE, Currency[].class);
        assertThat(currencies.length, equalTo(3));
        assertThat(currencies[0].getName(), equalTo("USD"));
        assertThat(currencies[0].getRate(), equalTo(1D));

        assertThat(currencies[1].getName(), equalTo("GBP"));
        assertThat(currencies[1].getRate(), equalTo(0.77244D));

        assertThat(currencies[2].getName(), equalTo("EUR"));
        assertThat(currencies[2].getRate(), equalTo(0.83893D));
    }
}