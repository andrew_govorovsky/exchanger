package com.govorovsky.exchanger.model;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by andrew on 02.09.17.
 */

@Dao
public interface CurrencyDao {
    @Query("SELECT * FROM currency")
    LiveData<List<Currency>> getAll();

    @Query("SELECT * FROM currency WHERE name= :name")
    Currency queryByName(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void updateCurrencies(Currency ...newCurrency);
}
