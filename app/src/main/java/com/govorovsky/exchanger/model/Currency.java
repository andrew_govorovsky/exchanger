package com.govorovsky.exchanger.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by andrew on 02.09.17.
 */

@Entity
public class Currency {
    private static final String COLUMN_NAME_RATE = "rate";
    private static final String COLUMN_NAME_NAME = "name";

    public Currency(String name, double rate) {
        this.name = name;
        this.rate = rate;
    }

    @PrimaryKey
    @ColumnInfo(name = COLUMN_NAME_NAME)
    public final String name;

    @ColumnInfo(name = COLUMN_NAME_RATE)
    private final double rate;

    public String getName() {
        java.util.Currency.getInstance("USD");
        return name;
    }


    public double getRate() {
        return rate;
    }
}
