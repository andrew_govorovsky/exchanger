package com.govorovsky.exchanger.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by andrew on 02.09.17.
 */

@Database(entities = {Currency.class}, version = 1)
public abstract class CurrencyDatabase extends RoomDatabase {
    private static CurrencyDatabase sInstance;

    public abstract CurrencyDao getCurrencyDao();

    public static synchronized CurrencyDatabase getInstance(Context context) {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(context, CurrencyDatabase.class, "currency.db").build();
        }
        return sInstance;
    }
}
