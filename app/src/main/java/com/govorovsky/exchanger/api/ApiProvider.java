package com.govorovsky.exchanger.api;

import android.support.annotation.VisibleForTesting;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.govorovsky.exchanger.model.Currency;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andrew on 03.09.17.
 */

public class ApiProvider {
    private CurrencyRatesService mRatesService;
    private String mBaseUrl = "http://api.fixer.io";

    public ApiProvider() {
        initService();
    }

    @VisibleForTesting
    public ApiProvider(String baseUrl) {
        mBaseUrl = baseUrl;
        initService();
    }

    public CurrencyRatesService getCurrencyRatesService() {
        return mRatesService;
    }

    public void initService() {
        Gson gson = new GsonBuilder().registerTypeAdapter(Currency[].class, new CurrencyJsonDeserializer()).create();
        mRatesService = new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(CurrencyRatesService.class);
    }

}
