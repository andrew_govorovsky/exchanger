package com.govorovsky.exchanger.api;

import com.govorovsky.exchanger.model.Currency;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by andrew on 02.09.17.
 */

public interface CurrencyRatesService {

    @GET("/latest?base=USD")
    Call<Currency[]> getLatestCurrencyRates();
}
