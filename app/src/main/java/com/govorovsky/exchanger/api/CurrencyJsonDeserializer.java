package com.govorovsky.exchanger.api;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.govorovsky.exchanger.model.Currency;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * Created by andrew on 02.09.17.
 */

public class CurrencyJsonDeserializer implements JsonDeserializer<Currency[]> {

    private static final Set<String> sSupportedCurrencies = new HashSet<String>() {{
        add("USD");
        add("GBP");
        add("EUR");
    }};

    @Override
    public Currency[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        ArrayList<Currency> currencies = new ArrayList<>();
        JsonObject currencyDesc = json.getAsJsonObject();
        currencies.add(new Currency(currencyDesc.get("base").getAsString(), 1D)); // base currency with rate 1.0
        JsonObject rates = currencyDesc.getAsJsonObject("rates");
        for (Map.Entry<String, JsonElement> entry : rates.entrySet()) {
            if (sSupportedCurrencies.contains(entry.getKey())) {
                currencies.add(new Currency(entry.getKey(), entry.getValue().getAsDouble()));
            }
        }
        return currencies.toArray(new Currency[sSupportedCurrencies.size()]);
    }
}
