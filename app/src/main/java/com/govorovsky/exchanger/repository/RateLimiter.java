package com.govorovsky.exchanger.repository;

import android.os.SystemClock;

import java.util.concurrent.TimeUnit;

/**
 * Created by andrew on 05.09.17.
 */

class RateLimiter {

    private Long mLastAccessTime = null;
    private final Long mTimeout;

    RateLimiter(Long timeout, TimeUnit timeUnit) {
        mTimeout = timeUnit.toMillis(timeout);
    }

    synchronized boolean isTimeoutExpired() {
        long timeNow = SystemClock.uptimeMillis();
        if (mLastAccessTime == null) {
            mLastAccessTime = timeNow;
            return true;
        } else if (timeNow - mLastAccessTime > mTimeout) {
            mLastAccessTime = timeNow;
            return true;
        }
        return false;
    }

    void reset() {
        mLastAccessTime = null;
    }

}
