package com.govorovsky.exchanger.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.govorovsky.exchanger.api.ApiProvider;
import com.govorovsky.exchanger.model.Currency;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by andrew on 03.09.17.
 */

class RemoteCurrencyRatesDataSource implements CurrencyRatesDataSource<LiveData<Result<List<Currency>>>> {

    private final AppExecutors mAppExecutors;
    private final ApiProvider mProvider;

    RemoteCurrencyRatesDataSource(ApiProvider provider, AppExecutors appExecutors) {
        mAppExecutors = appExecutors;
        mProvider = provider;
    }

    @Override
    public LiveData<Result<List<Currency>>> getLatestCurrencyRates() {
        return new OneShotLiveData<Result<List<Currency>>>(mAppExecutors.networkIO()) {
            @Override
            @NonNull
            protected Result<List<Currency>> fetchData() {
                Result<List<Currency>> mResult;
                Call<Currency[]> call = mProvider.getCurrencyRatesService().getLatestCurrencyRates();
                try {
                    Response<Currency[]> response = call.execute();
                    if (response.isSuccessful() && response.body() != null) {
                        mResult = Result.ok(Collections.unmodifiableList(Arrays.asList(response.body())));
                    } else {
                        mResult = Result.error(null, "Api error");
                    }
                } catch (Exception e) {
                    Log.e("RemoteDataSource", "Error", e);
                    mResult = Result.error(null, "Network error");
                }
                return mResult;
            }
        };
    }

    @Override
    public void updateLatestCurrencyRates(Currency... newRates) {
        throw new UnsupportedOperationException("Server read only");
    }


    private static abstract class OneShotLiveData<T> extends LiveData<T> {
        private final AtomicBoolean mWasExecuted = new AtomicBoolean();
        private final Executor mExecutor;

        OneShotLiveData(Executor executor) {
            mExecutor = executor;
        }

        @Override
        protected void onActive() {
            super.onActive();
            if (mWasExecuted.compareAndSet(false, true)) {
                mExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        postValue(fetchData());
                    }
                });
            }
        }

        @NonNull
        protected abstract T fetchData();
    }
}
