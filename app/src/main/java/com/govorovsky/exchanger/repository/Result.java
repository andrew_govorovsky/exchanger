package com.govorovsky.exchanger.repository;

import android.support.annotation.Nullable;

import static com.govorovsky.exchanger.repository.Result.Status.ERROR;
import static com.govorovsky.exchanger.repository.Result.Status.OK;

/**
 * Created by andrew on 03.09.17.
 */

public class Result<T> {

    private final Status mStatus;

    private final T data;

    @Nullable
    private final String mMessage;


    public Result(Status status, T dataValue, String message) {
        mStatus = status;
        data = dataValue;
        mMessage = message;
    }

    public Status getStatus() {
        return mStatus;
    }

    public T getData() {
        return data;
    }

    @Nullable
    public String getMessage() {
        return mMessage;
    }

    static <T> Result<T> ok(T data) {
        return new Result<>(OK, data, null);
    }

    static <T> Result<T> error(T data, String msg) {
        return new Result<>(ERROR, data, msg);
    }

    enum Status {
        OK,
        ERROR
    }
}
