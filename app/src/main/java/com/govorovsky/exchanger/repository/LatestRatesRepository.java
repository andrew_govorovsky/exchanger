package com.govorovsky.exchanger.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.govorovsky.exchanger.api.ApiProvider;
import com.govorovsky.exchanger.model.Currency;
import com.govorovsky.exchanger.model.CurrencyDatabase;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by andrew on 03.09.17.
 */

public class LatestRatesRepository {
    private static LatestRatesRepository sInstance;
    private final CurrencyRatesDataSource<LiveData<Result<List<Currency>>>> mLocalDataSource;
    private final CurrencyRatesDataSource<LiveData<Result<List<Currency>>>> mRemoteDataSource;
    private final AppExecutors mAppExecutors = new AppExecutors();
    private final RateLimiter mRateLimiter;
    private static final long POLLING_TIMEOUT = 30L;
    private static final long RATE_LIMIT = POLLING_TIMEOUT;

    @VisibleForTesting
    LatestRatesRepository(CurrencyRatesDataSource<LiveData<Result<List<Currency>>>> localDataSource,
                                  CurrencyRatesDataSource<LiveData<Result<List<Currency>>>> remoteDataSource) {
        this(localDataSource, remoteDataSource, new RateLimiter(RATE_LIMIT, TimeUnit.SECONDS), true);
    }

    public static synchronized LatestRatesRepository getInstance(Context appContext) {
        if (sInstance == null) {
            AppExecutors appExecutors = new AppExecutors();
            LocalCurrencyRatesDataSource localSource = new LocalCurrencyRatesDataSource(CurrencyDatabase.getInstance(appContext), appExecutors);
            RemoteCurrencyRatesDataSource remoteSource = new RemoteCurrencyRatesDataSource(new ApiProvider(), appExecutors);
            sInstance = new LatestRatesRepository(localSource, remoteSource);
        }
        return sInstance;
    }

    @VisibleForTesting
    LatestRatesRepository(CurrencyRatesDataSource<LiveData<Result<List<Currency>>>> localDataSource,
                          CurrencyRatesDataSource<LiveData<Result<List<Currency>>>> remoteDataSource,
                          RateLimiter limiter, boolean enablePolling) {
        mLocalDataSource = localDataSource;
        mRemoteDataSource = remoteDataSource;
        mRateLimiter = limiter;
        if (enablePolling) {
            startPollingRefresh();
        }
    }


    public LiveData<Result<List<Currency>>> getLatestCurrencyRates() {
        final MediatorLiveData<Result<List<Currency>>> result = new MediatorLiveData<>();
        result.addSource(mLocalDataSource.getLatestCurrencyRates(), new Observer<Result<List<Currency>>>() {
            @Override
            public void onChanged(@Nullable Result<List<Currency>> dbResult) {
                if (shouldFetchNewData(dbResult)) {
                    fetchNewData(result, dbResult);
                } else {
                    result.setValue(dbResult);
                }
            }
        });
        return result;
    }

    private void fetchNewData(final MediatorLiveData<Result<List<Currency>>> result, final Result<List<Currency>> dbResult) {
        final LiveData<Result<List<Currency>>> remoteData = mRemoteDataSource.getLatestCurrencyRates();
        result.addSource(remoteData, new Observer<Result<List<Currency>>>() {
            @Override
            public void onChanged(@Nullable Result<List<Currency>> networkResult) {
                result.removeSource(remoteData);
                if (isResultOk(networkResult)) {
                    mLocalDataSource.updateLatestCurrencyRates(networkResult.getData().toArray(new Currency[0]));
                } else {
                    // just return old cached data, we will try to update it on next request
                    mRateLimiter.reset();
                    result.setValue(Result.error(dbResult.getData(), "Network error"));
                }
            }
        });
    }

    private boolean isResultOk(@Nullable Result<List<Currency>> result) {
        return result != null && result.getStatus() == Result.Status.OK;
    }

    private boolean shouldFetchNewData(@Nullable Result<List<Currency>> dbResult) {
        return mRateLimiter.isTimeoutExpired() || dbResult == null || dbResult.getData().isEmpty();
    }

    private void startPollingRefresh() {
        mAppExecutors.scheduled().scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        final LiveData<Result<List<Currency>>> liveData = getLatestCurrencyRates();
                        liveData.observeForever(new Observer<Result<List<Currency>>>() {
                            @Override
                            public void onChanged(@Nullable Result<List<Currency>> result) {
                                liveData.removeObserver(this);
                            }
                        });
                    }
                });
            }
        }, 0, POLLING_TIMEOUT, TimeUnit.SECONDS);
    }
}
