package com.govorovsky.exchanger.repository;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.content.Context;
import android.support.annotation.Nullable;

import com.govorovsky.exchanger.model.Currency;
import com.govorovsky.exchanger.model.CurrencyDatabase;

import java.util.List;

/**
 * Created by andrew on 03.09.17.
 */

class LocalCurrencyRatesDataSource implements CurrencyRatesDataSource<LiveData<Result<List<Currency>>>> {

    private final CurrencyDatabase mCurrencyDatabase;
    private final AppExecutors mAppExecutors;

    LocalCurrencyRatesDataSource(CurrencyDatabase database, AppExecutors appExecutors) {
        mCurrencyDatabase = database;
        mAppExecutors = appExecutors;
    }

    @Override
    public LiveData<Result<List<Currency>>> getLatestCurrencyRates() {
        return Transformations.map(mCurrencyDatabase.getCurrencyDao().getAll(), new Function<List<Currency>, Result<List<Currency>>>() {
            @Override
            public Result<List<Currency>> apply(List<Currency> input) {
                return input != null ? Result.ok(input) : Result.<List<Currency>>error(null, "db error");
            }
        });
    }

    @Override
    public void updateLatestCurrencyRates(final Currency... newRates) {
        mAppExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mCurrencyDatabase.getCurrencyDao().updateCurrencies(newRates);
            }
        });
    }
}
