package com.govorovsky.exchanger.repository;

import android.arch.lifecycle.LiveData;

import com.govorovsky.exchanger.model.Currency;


/**
 * Created by andrew on 03.09.17.
 */

public interface CurrencyRatesDataSource<T extends LiveData<?>> {
    T getLatestCurrencyRates();

    void updateLatestCurrencyRates(Currency... newRates);
}
