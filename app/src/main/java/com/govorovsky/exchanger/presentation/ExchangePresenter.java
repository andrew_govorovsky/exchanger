package com.govorovsky.exchanger.presentation;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.govorovsky.exchanger.model.Currency;
import com.govorovsky.exchanger.repository.LatestRatesRepository;
import com.govorovsky.exchanger.repository.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by andrew on 06.09.17.
 */

public class ExchangePresenter implements ExchangeScreenPresenter, Observer<Result<List<Currency>>> {

    private View mView;
    private final LatestRatesRepository mLatestRatesRepository;
    private LiveData<Result<List<Currency>>> mResultLiveData;
    private CurrencyViewModel mCurrentFrom;
    private CurrencyViewModel mCurrentTo;
    private HashMap<String, Double> mCurrencyHashMap = new HashMap<>();

    public ExchangePresenter(View view, LatestRatesRepository latestRatesRepository) {
        mView = view;
        mLatestRatesRepository = latestRatesRepository;
        mResultLiveData = mLatestRatesRepository.getLatestCurrencyRates();
        mResultLiveData.observeForever(this);
    }

    @Override
    public void onFromCurrencySelected(CurrencyViewModel from) {
        mCurrentFrom = from;
        if (mCurrentTo != null) {
            updateCurrencyRateSummary();
            mCurrentFrom.setAmount(convertCurrency(mCurrentTo.getISOCode(), mCurrentFrom.getISOCode(), mCurrentTo.getAmount()));
        }
        mView.updateCurrencyRateSummary();
        mView.setActiveFrom();
    }


    @Override
    public void onToCurrencySelected(CurrencyViewModel to) {
        mCurrentTo = to;
        if (mCurrentFrom != null) {
            updateCurrencyRateSummary();
            mCurrentTo.setAmount(convertCurrency(mCurrentFrom.getISOCode(), mCurrentTo.getISOCode(), mCurrentFrom.getAmount()));
        }
        mView.updateCurrencyRateSummary();
        mView.setActiveTo();
    }

    private void updateCurrencyRateSummary() {
        mCurrentFrom.setCurrentRate(getRateRepresentation(mCurrentFrom, mCurrentTo));
        mCurrentTo.setCurrentRate(getRateRepresentation(mCurrentTo, mCurrentFrom));
    }

    @Override
    public void onCurrencyFromChanged(CurrencyViewModel changed, double newValue) {
        changed.setAmount(newValue);
        mCurrentTo.setAmount(convertCurrency(mCurrentFrom.getISOCode(), mCurrentTo.getISOCode(), mCurrentFrom.getAmount()));
        mView.updateCurrencyRateSummary();
        mView.setActiveFrom();
    }

    @Override
    public void onCurrencyToChanged(CurrencyViewModel changed, double newValue) {
        changed.setAmount(newValue);
        mCurrentFrom.setAmount(convertCurrency(mCurrentTo.getISOCode(), mCurrentFrom.getISOCode(), mCurrentTo.getAmount()));
        mView.updateCurrencyRateSummary();
        mView.setActiveTo();
    }

    private String getRateRepresentation(CurrencyViewModel from, CurrencyViewModel to) {
        return String.format(Locale.US, "1%s = %.4f%s", from.getRepresentation(), convertCurrency(from.getISOCode(), to.getISOCode(), 1), to.getRepresentation());
    }

    private double convertCurrency(String from, String to, double amnt) {
        return amnt / mCurrencyHashMap.get(from) * mCurrencyHashMap.get(to);
    }


    @Override
    public void onDestroy() {
        mResultLiveData.removeObserver(this);
    }


    @Override
    public void onChanged(@Nullable Result<List<Currency>> result) {
        if (result != null) {
            boolean firstLoad = mCurrencyHashMap.isEmpty();
            for (Currency currency : result.getData()) {
                mCurrencyHashMap.put(currency.getName(), currency.getRate());
            }
            if (firstLoad) {
                ArrayList<CurrencyViewModel> viewModelList = transformFromModel(result.getData());
                mView.setCurrencies(viewModelList, viewModelList);
                mView.setActiveFrom();
            } else {
                updateCurrencyRateSummary();
                mView.updateCurrencyRateSummary();
            }
        }
    }

    private ArrayList<CurrencyViewModel> transformFromModel(List<Currency> currencies) {
        ArrayList<CurrencyViewModel> list = new ArrayList<>(currencies.size());
        for (Currency currency : currencies) {
            list.add(new CurrencyViewModel(currency.getName(), 0D));
        }
        return list;
    }
}
