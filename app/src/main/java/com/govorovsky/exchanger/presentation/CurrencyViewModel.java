package com.govorovsky.exchanger.presentation;


import java.util.Currency;
import java.util.Locale;

/**
 * Created by andrew on 06.09.17.
 */

public class CurrencyViewModel {
    private final String mRepresentation;
    private final String mISOCode;
    private double mAmount;
    private String mCurrentRate;

    public CurrencyViewModel(String ISOCode, double amount) {
        mRepresentation = Currency.getInstance(ISOCode).getSymbol(Locale.US);
        mISOCode = ISOCode;
        mAmount = amount;
    }

    public String getRepresentation() {
        return mRepresentation;
    }

    public String getISOCode() {
        return mISOCode;
    }

    public double getAmount() {
        return mAmount;
    }

    public String getCurrentRate() {
        return mCurrentRate;
    }

    public void setCurrentRate(String currentRate) {
        mCurrentRate = currentRate;
    }

    public void setAmount(double amount) {
        mAmount = amount;
    }
}
