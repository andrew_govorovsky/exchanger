package com.govorovsky.exchanger.presentation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrew on 05.09.17.
 */

interface ExchangeScreenPresenter {

    void onFromCurrencySelected(CurrencyViewModel from);

    void onToCurrencySelected(CurrencyViewModel to);

    void onCurrencyFromChanged(CurrencyViewModel changed, double newValue);

    void onCurrencyToChanged(CurrencyViewModel changed, double newValue);

    void onDestroy();

    interface View {
        void updateCurrencyRateSummary();

        void setCurrencies(ArrayList<CurrencyViewModel> from, ArrayList<CurrencyViewModel> to);

        void setActiveTo();

        void setActiveFrom();
    }

}
