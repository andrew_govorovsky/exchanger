package com.govorovsky.exchanger.presentation;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.govorovsky.exchanger.R;
import com.govorovsky.exchanger.presentation.CurrencyViewModel;
import com.govorovsky.exchanger.presentation.ExchangePresenter;
import com.govorovsky.exchanger.presentation.ExchangeScreenPresenter;
import com.govorovsky.exchanger.repository.LatestRatesRepository;

import java.util.ArrayList;
import java.util.Locale;

public class ExchangeActivity extends AppCompatActivity implements ExchangeScreenPresenter.View {

    private ViewPager mPagerFrom;
    private ViewPager mPagerTo;
    private CurrencyAdapter mFromAdapter;
    private CurrencyAdapter mToAdapter;
    private ExchangeScreenPresenter mExchangeScreenPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);
        mPagerFrom = (ViewPager) findViewById(R.id.currency_from);
        mPagerTo = (ViewPager) findViewById(R.id.currency_to);
        mFromAdapter = new CurrencyAdapter(mPagerFrom, Destination.FROM);
        mPagerFrom.setAdapter(mFromAdapter);
        mToAdapter = new CurrencyAdapter(mPagerTo, Destination.TO);
        mPagerTo.setAdapter(mToAdapter);
        mPagerFrom.addOnPageChangeListener(new FromPageChangeListener());
        mPagerTo.addOnPageChangeListener(new ToPageChangeListener());
        mExchangeScreenPresenter = new ExchangePresenter(this, LatestRatesRepository.getInstance(getApplicationContext()));
    }

    @Override
    protected void onDestroy() {
        mExchangeScreenPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void updateCurrencyRateSummary() {
        mToAdapter.notifyDataSetChanged();
        mFromAdapter.notifyDataSetChanged();
    }

    @Override
    public void setCurrencies(ArrayList<CurrencyViewModel> from, ArrayList<CurrencyViewModel> to) {
        mFromAdapter.setCurrency(from);
        mToAdapter.setCurrency(to);
        mExchangeScreenPresenter.onFromCurrencySelected(from.get(mPagerFrom.getCurrentItem()));
        mExchangeScreenPresenter.onToCurrencySelected(from.get(mPagerTo.getCurrentItem()));
    }

    @Override
    public void setActiveTo() {
        mPagerTo.requestFocus();
    }

    @Override
    public void setActiveFrom() {
        mPagerFrom.requestFocus();
    }


    private class CurrencyAdapter extends PagerAdapter {
        private ArrayList<CurrencyViewModel> mCurrencyViewModels = new ArrayList<>();
        private Handler mHandler = new Handler(Looper.getMainLooper());
        private Runnable workRunnable;
        private Destination mDestination;
        private ViewPager mParent;

        public CurrencyAdapter(ViewPager parent, Destination destination) {
            mDestination = destination;
            mParent = parent;
        }

        public void setCurrency(ArrayList<CurrencyViewModel> currency) {
            mCurrencyViewModels = currency;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mCurrencyViewModels.size();
        }

        public ArrayList<CurrencyViewModel> getCurrencyViewModels() {
            return mCurrencyViewModels;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View item = getLayoutInflater().inflate(R.layout.currency_item, container, false);
            bindView(item, mCurrencyViewModels.get(position));
            container.addView(item);
            return item;
        }

        private void bindView(View item, CurrencyViewModel model) {
            ((TextView) item.findViewById(R.id.name)).setText(model.getISOCode());
            EditText editText = (EditText) item.findViewById(R.id.value);
            editText.setText(model.getAmount() > 0 ? formatCurrencyValue(model) : "");
            editText.addTextChangedListener(new CurrencyTextWatcher());
            if (!TextUtils.isEmpty(model.getCurrentRate())) {
                ((TextView) item.findViewById(R.id.rate)).setText(String.valueOf(model.getCurrentRate()));
            }
        }

        private String formatCurrencyValue(CurrencyViewModel model) {
            return String.format(Locale.US, "%.2f", model.getAmount()).replaceAll("\\.?0*$", "");
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }


        private class CurrencyTextWatcher implements TextWatcher {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                mHandler.removeCallbacks(workRunnable);
                final CurrencyViewModel changed = getCurrencyViewModels().get(mParent.getCurrentItem());
                if (!TextUtils.isEmpty(s)) {
                    workRunnable = new Runnable() {
                        @Override
                        public void run() {
                            double newValue = Double.parseDouble(s.toString());
                            if (mDestination == Destination.FROM) {
                                mExchangeScreenPresenter.onCurrencyFromChanged(changed, newValue);
                            } else {
                                mExchangeScreenPresenter.onCurrencyToChanged(changed, newValue);
                            }
                        }
                    };
                    mHandler.postDelayed(workRunnable, 300);
                }
            }
        }
    }

    private enum Destination {
        FROM, TO
    }

    private class FromPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            mExchangeScreenPresenter.onFromCurrencySelected(mFromAdapter.getCurrencyViewModels().get(position));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    private class ToPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            mExchangeScreenPresenter.onToCurrencySelected(mToAdapter.getCurrencyViewModels().get(position));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
