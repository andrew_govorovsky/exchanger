package com.govorovsky.exchanger.repository;

/**
 * Created by andrew on 03.09.17.
 */

class Responses {
    static final double EUR_INITIAL_VALUE = 0.83893;
    static final String RESPONSE_OK = "{\"base\":\"USD\",\"date\":\"2017-09-01\",\"rates\":{\"AUD\":1.2602,\"BGN\":1.6408,\"BRL\":3.1395,\"CAD\":1.2441,\"CHF\":0.95982,\"CNY\":6.5591,\"CZK\":21.877,\"DKK\":6.2398,\"GBP\":0.77244,\"HKD\":7.8248,\"HRK\":6.2261,\"HUF\":255.95,\"IDR\":13318.0,\"ILS\":3.5737,\"INR\":64.034,\"JPY\":110.14,\"KRW\":1120.3,\"MXN\":17.836,\"MYR\":4.2705,\"NOK\":7.7647,\"NZD\":1.3958,\"PHP\":51.091,\"PLN\":3.5576,\"RON\":3.856,\"RUB\":57.737,\"SEK\":7.9512,\"SGD\":1.3545,\"THB\":33.17,\"TRY\":3.438,\"ZAR\":12.935," +
            String.format("\"EUR\":%f}},", EUR_INITIAL_VALUE);
    static final double EUR_NEW_VALUE = 0.666;
    static final String RESPONSE_OK_CHANGED = "{\"base\":\"USD\",\"date\":\"2017-09-01\",\"rates\":{\"AUD\":1.2602,\"BGN\":1.6408,\"BRL\":3.1395,\"CAD\":1.2441,\"CHF\":0.95982,\"CNY\":6.5591,\"CZK\":21.877,\"DKK\":6.2398,\"GBP\":0.77244,\"HKD\":7.8248,\"HRK\":6.2261,\"HUF\":255.95,\"IDR\":13318.0,\"ILS\":3.5737,\"INR\":64.034,\"JPY\":110.14,\"KRW\":1120.3,\"MXN\":17.836,\"MYR\":4.2705,\"NOK\":7.7647,\"NZD\":1.3958,\"PHP\":51.091,\"PLN\":3.5576,\"RON\":3.856,\"RUB\":57.737,\"SEK\":7.9512,\"SGD\":1.3545,\"THB\":33.17,\"TRY\":3.438,\"ZAR\":12.935," +
            String.format("\"EUR\":%f}},", EUR_NEW_VALUE);
    static final String BAD_RESPONSE = "{}";
}
