package com.govorovsky.exchanger.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.util.Pair;

import com.govorovsky.exchanger.model.Currency;
import com.govorovsky.exchanger.model.CurrencyDatabase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class LocalDataSourceTest {

    private CurrencyDatabase mCurrencyDatabase;

    @Before
    public void initDd() {
        mCurrencyDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getTargetContext(), CurrencyDatabase.class)
                .build();
    }

    @Test
    public void testInsertAndUpdate() throws Exception {
        Currency[] newCurrencies = newCurrencies(new Pair<>("USD", 1D), new Pair<>("GBP", 341D), new Pair<>("EUR", 888D));
        mCurrencyDatabase.getCurrencyDao().updateCurrencies(newCurrencies);
        List<Currency> currencies = SynchronousLiveData.from(mCurrencyDatabase.getCurrencyDao().getAll()).getValue();
        assertNotNull(currencies);
        assertThat(currencies.size(), equalTo(3));
        assertThat(mCurrencyDatabase.getCurrencyDao().queryByName("USD").getRate(), equalTo(1D));
        assertThat(mCurrencyDatabase.getCurrencyDao().queryByName("GBP").getRate(), equalTo(341D));
        assertThat(mCurrencyDatabase.getCurrencyDao().queryByName("EUR").getRate(), equalTo(888D));
        mCurrencyDatabase.getCurrencyDao().updateCurrencies(newCurrencies(new Pair<>("GBP", 666D)));
        assertThat(mCurrencyDatabase.getCurrencyDao().queryByName("USD").getRate(), equalTo(1D));
        assertThat(mCurrencyDatabase.getCurrencyDao().queryByName("GBP").getRate(), equalTo(666D));
        assertThat(mCurrencyDatabase.getCurrencyDao().queryByName("EUR").getRate(), equalTo(888D));
    }

    @NonNull
    private Currency[] newCurrencies(Pair<String, Double>... currs) {
        Currency[] currencies = new Currency[currs.length];
        for (int i = 0; i < currs.length; i++) {
            currencies[i] = new Currency(currs[i].first, currs[i].second);
        }
        return currencies;
    }

    @After
    public void release() {
        mCurrencyDatabase.close();
    }
}
