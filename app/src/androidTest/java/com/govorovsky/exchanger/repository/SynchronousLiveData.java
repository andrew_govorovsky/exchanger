package com.govorovsky.exchanger.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import java.util.HashSet;
import java.util.concurrent.CountDownLatch;

/**
 * Created by andrew on 03.09.17.
 */
class SynchronousLiveData<T> extends LiveData<T> {

    private final LiveData<T> mWrapped;

    private SynchronousLiveData(LiveData<T> wrapped) {
        mWrapped = wrapped;
    }

    static <T> LiveData<T> from(LiveData<T> toWrap) {
        return new SynchronousLiveData<>(toWrap);
    }

    public T getValue() {
        final T[] val = (T[]) new Object[1];
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        mWrapped.observeForever(new Observer<T>() {
            @Override
            public void onChanged(@Nullable T t) {
                val[0] = t;
                mWrapped.removeObserver(this);
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
        }
        return val[0];
    }
}
