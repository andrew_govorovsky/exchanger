package com.govorovsky.exchanger.repository;

import android.arch.lifecycle.LiveData;
import android.support.test.runner.AndroidJUnit4;

import com.govorovsky.exchanger.api.ApiProvider;
import com.govorovsky.exchanger.model.Currency;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

/**
 * Created by andrew on 03.09.17.
 */

@RunWith(AndroidJUnit4.class)
public class RemoteDataSourceTest {
    private CurrencyRatesDataSource mRatesDataSource;
    private MockWebServer mMockWebServer;

    @Before
    public void setUp() throws IOException {
        mMockWebServer = new MockWebServer();
        mMockWebServer.start();
        String url = mMockWebServer.url("/").toString();
        mRatesDataSource = new RemoteCurrencyRatesDataSource(new ApiProvider(url), new AppExecutors());
    }

    @After
    public void tearDown() throws IOException {
        mMockWebServer.shutdown();
    }

    @Test
    public void testNetworkRequestOk() throws Exception {
        MockResponse mockResponse = new MockResponse();
        mockResponse.setBody(Responses.RESPONSE_OK);
        mMockWebServer.enqueue(mockResponse);
        final LiveData<Result<List<Currency>>> latestCurrencyRates = mRatesDataSource.getLatestCurrencyRates();
        Result<List<Currency>> listResponse = SynchronousLiveData.from(latestCurrencyRates).getValue();
        assertNotNull(listResponse);
        assertThat(listResponse.getStatus(), equalTo(Result.Status.OK));
        assertThat(listResponse.getData().size(), equalTo(3));
    }

    @Test
    public void testNetworkRequestError() throws Exception {
        MockResponse mockResponse = new MockResponse();
        mockResponse.setBody(Responses.BAD_RESPONSE);
        mMockWebServer.enqueue(mockResponse);
        final LiveData<Result<List<Currency>>> latestCurrencyRates = mRatesDataSource.getLatestCurrencyRates();
        Result<List<Currency>> value = SynchronousLiveData.from(latestCurrencyRates).getValue();
        assertNotNull(value);
        assertThat(value.getStatus(), equalTo(Result.Status.ERROR));
    }
}
