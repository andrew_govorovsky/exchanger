package com.govorovsky.exchanger.repository;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.govorovsky.exchanger.api.ApiProvider;
import com.govorovsky.exchanger.model.Currency;
import com.govorovsky.exchanger.model.CurrencyDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Created by andrew on 03.09.17.
 */

@RunWith(AndroidJUnit4.class)
public class LatestRatesRepositoryTest {
    private MockWebServer mMockWebServer;
    private LatestRatesRepository mLatestRatesRepository;
    private MockRateLimiter mRateLimiter;
    private LocalCurrencyRatesDataSource mLocalDataSource;
    private RemoteCurrencyRatesDataSource mRemoteDataSource;

    @Before
    public void setUp() throws IOException {
        mMockWebServer = new MockWebServer();
        mMockWebServer.start();
        AppExecutors appExecutors = new AppExecutors();
        mRateLimiter = new MockRateLimiter();
        mLocalDataSource = new LocalCurrencyRatesDataSource(Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getTargetContext(), CurrencyDatabase.class).build(),
                appExecutors);
        mRemoteDataSource = new RemoteCurrencyRatesDataSource(new ApiProvider(mMockWebServer.url("/").toString()), appExecutors);
        mLatestRatesRepository = new LatestRatesRepository(mLocalDataSource, mRemoteDataSource, mRateLimiter, false);
    }


    @Test
    public void testGetLatestRates() throws Exception {
        mMockWebServer.enqueue(new MockResponse().setBody(Responses.RESPONSE_OK));
        Result<List<Currency>> result = SynchronousLiveData.from(mLatestRatesRepository.getLatestCurrencyRates()).getValue();
        assertNotNull(result);
        assertThat(result.getStatus(), equalTo(Result.Status.OK));
    }

    @Test
    public void testGetLatestRatesAfterTimeout() throws Exception {
        mMockWebServer.enqueue(new MockResponse().setBody(Responses.RESPONSE_OK));
        Result<List<Currency>> result = SynchronousLiveData.from(mLatestRatesRepository.getLatestCurrencyRates()).getValue();
        assertThat(result.getData().get(2).getRate(), equalTo(Responses.EUR_INITIAL_VALUE));
        result = SynchronousLiveData.from(mLatestRatesRepository.getLatestCurrencyRates()).getValue();
        assertThat(result.getData().get(2).getRate(), equalTo(Responses.EUR_INITIAL_VALUE));

        //data has changed on server
        mRateLimiter.setTimeoutExpired(true);
        mMockWebServer.enqueue(new MockResponse().setBody(Responses.RESPONSE_OK_CHANGED));
        result = SynchronousLiveData.from(mLatestRatesRepository.getLatestCurrencyRates()).getValue();
        assertThat(result.getData().get(2).getRate(), equalTo(Responses.EUR_NEW_VALUE));
        result = SynchronousLiveData.from(mLocalDataSource.getLatestCurrencyRates()).getValue();
        assertThat(result.getData().get(2).getRate(), equalTo(Responses.EUR_NEW_VALUE));
    }

    class MockRateLimiter extends RateLimiter {

        private boolean mIsExpired = true;

        MockRateLimiter() {
            super(30L, TimeUnit.SECONDS);
        }

        @Override
        synchronized boolean isTimeoutExpired() {
            if (mIsExpired) {
                mIsExpired = false;
                return true;
            } else {
                return false;
            }
        }

        synchronized void setTimeoutExpired(boolean isExpired) {
            mIsExpired = isExpired;
        }

    }
}
